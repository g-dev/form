 $(document).ready(function (){       
        var arrayEstadosJSON = [{
	"Sigla": "AC",
	"Nome": "Acre"
},
     {
	"Sigla": "AL",
	"Nome": "Alagoas"
},
     {
	"Sigla": "AM",
	"Nome": "Amazonas"
},
     {
	"Sigla": "AP",
	"Nome": "Amapá"
},
     {
	"Sigla": "BA",
	"Nome": "Bahia"
},
     {
	"Sigla": "CE",
	"Nome": "Ceará"
},
     {
	"Sigla": "DF",
	"Nome": "Distrito Federal"
},
     {
	"Sigla": "ES",
	"Nome": "Espírito Santo"
},
     {
	"Sigla": "GO",
	"Nome": "Goiás"
},
     {
	"Sigla": "MA",
	"Nome": "Maranhão"
},
     {
	"Sigla": "MG",
	"Nome": "Minas Gerais"
},
     {
	"Sigla": "MS",
	"Nome": "Mato Grosso do Sul"
},
     {
	"Sigla": "MT",
	"Nome": "Mato Grosso"
},
     {
	"Sigla": "PA",
	"Nome": "Pará"
},
     {
	"Sigla": "PB",
	"Nome": "Paraíba"
},
     {
	"Sigla": "PE",
	"Nome": "Pernambuco"
},
     {
	"Sigla": "PI",
	"Nome": "Piauí"
},
     {
	"Sigla": "PR",
	"Nome": "Paraná"
},
     {
	"Sigla": "RJ",
	"Nome": "Rio de Janeiro"
},
     {
	"Sigla": "RN",
	"Nome": "Rio Grande do Norte"
},
     {
	"Sigla": "RO",
	"Nome": "Rondônia"
},
     {
	"Sigla": "RR",
	"Nome": "Roraima"
},
     {
	"Sigla": "RS",
	"Nome": "Rio Grande do Sul"
},
     {
	"Sigla": "SC",
	"Nome": "Santa Catarina"
},
     {
	"Sigla": "SE",
	"Nome": "Sergipe"
},
     {
	"Sigla": "SP",
	"Nome": "São Paulo"
},
     {
	"Sigla": "TO",
	"Nome": "Tocantins"
}]


$.each(arrayEstadosJSON, function(i, item){ 
var objeto = new window.Option(item['Nome'],item['Sigla']);
$("#estados").append(objeto);  
});

$('form').submit(function (e){
   e.preventDefault();
   
   var errorStack = '';
   if(!isNaN($("#nome").val())){
      errorStack += '[ERRO] O valor informado em Nome é numérico\n';
   }
   if(!isNaN($("#cidade").val())){
       errorStack += '[ERRO] O valor informado em Cidade é numérico\n';
   }
   if(isNaN($("#telefone").val())){
       errorStack += '[ERRO] O valor informado em Telefone não é numérico\n';
   }
   if($("#telefone").val().length<8){
       errorStack += '[ERRO] O valor informado em Telefone possui menos de 8 digitos\n'
   }
   if(isNaN($("#celular").val())){
       errorStack += '[ERRO] O valor informado em Celular não é numérico\n';
   }
   if($("#celular").val().length<8){
       errorStack += '[ERRO] O valor informado em Celular possui menos de 8 digitos\n'
   }
   
  if(errorStack == '') {
      this.submit();
      alert('Submetido com sucesso');
  }else {
        alert(errorStack);
    }
   
});

});