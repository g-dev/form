<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Formulário</title>
        <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="js/custom.js"></script>
    </head>
    <body>
        <form method="POST" action='index.php'>
            <div class="offset-md-4 form-group col-md-4" align="center">
            <label>Nome</label>
            <input type="text" name="nome" id="nome" class="form-control" required/>
            </div>
            
            <div class="offset-md-4 form-group col-md-4" align="center">
                <label>Email</label>
                <input type="email" name="email" id="email" class="form-control" required/>
            </div>
            
            <div class="offset-md-4 form-group col-md-4" align="center">
                
                   <label class="col-sm-5" style="text-align: center">Telefone</label>
                   <label class="col-sm-5" style="text-align: center">Celular</label>
                   <input type="text" name="telefone" id="telefone" class="col-sm-5"/>  
                   <input type="text" name="celular" id="celular" class="col-sm-5" required/>
            
            </div>
            <div class="offset-md-4 form-group col-md-4">
                <label class="offset-md-5">Cidade</label>
                <input type="text" name="cidade" id="cidade" class=" form-control" required/>
            </div>
            
             <div class="offset-md-4 form-group col-md-4">
                <label class="offset-md-5">Estado</label>
                <select id="estados" name='estados' class=" form-control" required/> <!-- Options adicionados via jQuery, ver /js/custom.js --></select>
            </div>
            
            <div class="offset-md-4 form-group col-md-4">
                <label class="offset-md-5">Mensagem</label>
                <textarea type="text" name="mensagem" id="mensagem" class=" form-control" required></textarea>
            </div>
            
            <div class="offset-md-4 form-group col-md-4">
                <label>Deseja contato via whatsapp?</label>
                <label>Sim &nbsp;</label><input type="radio" name='btn' value='s' id="contato" required/>
                <label>Não &nbsp;</label><input type="radio" name='btn' value='n' id="contato" required/>
            </div>
            
             <div class="offset-md-4 form-group col-md-4" align='center'>
               
                        <button type="submit" class="btn btn-default btn-success">Enviar</button>
               
            </div>
        </form>
    </body>
</html>
